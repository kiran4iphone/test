//
//  PagesCell.swift
//  Test
//
//  Created by VMK IT Services PVT LTD on 29/09/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit

class PagesCell: UITableViewCell {

    @IBOutlet weak var img_Profile: UIImageView!
    
    @IBOutlet weak var lbl_ProfileName: UILabel!
    
    @IBOutlet weak var lbl_ProfileDescrption: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func textFieldDidEndEditing_Action(_ sender: UITextField) {
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
