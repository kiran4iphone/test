//
//  ViewController.swift
//  Test
//
//  Created by VMK IT Services PVT LTD on 28/09/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit




class ViewController: UIViewController {
    //TODO:- IBOutlet's
    @IBOutlet weak var tbl_Pages: UITableView!
    //TODO:- Variables
    let cellId = "PagesCell"
    var pages_Arr = [Any]()
    
    //MARK:- View Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Instance Functions
    func setUp(){
        self.tbl_Pages.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        self.tbl_Pages.rowHeight = UITableViewAutomaticDimension
        self.tbl_Pages.estimatedRowHeight = 200
        self.tbl_Pages.tableFooterView = UIView.init(frame: CGRect.zero)
        
    }
    func reload(){
        self.pages_Arr.removeAll()
        self.tbl_Pages.reloadData()
    }
    //MARK:- UITextField Delegate
    @IBAction func textFieldDidEditingChanged_Action(_ sender: UITextField) {
        if sender.text == ""{
            reload()
            return
        }
        getDetailsFromService(searchText: sender.text ?? "")
    }
    
    //MARK:- API
    func getDetailsFromService(searchText:String){
        // url string...
        let urlString = "https://en.wikipedia.org//w/api.php?action=query&format=json&prop=pageimages%7Cpageterms&generator=prefixsearch&redirects=1&formatversion=2&piprop=thumbnail&pithumbsize=50&pilimit=20&wbptterms=description&gpssearch=\(searchText)&gpslimit=10"
        
        // session creations...
        let defaultConfiure = URLSessionConfiguration.default
        let defaultSession = URLSession.init(configuration: defaultConfiure, delegate: nil, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: URL.init(string: urlString)!) { (data, response, error) in
            
            // final response getting...
            if error != nil {
                print(error?.localizedDescription ?? "")
                
            }
            else {
                do {
                    // if its no results...
                    
                    let  jsonDict = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    
                    //  print(jsonDict as Any)
                    // if data avalaibles
                    if let complete = jsonDict?["batchcomplete"] as? Int,complete == 1{
                        
                        if let query_Dict = (jsonDict?["query"] as? NSDictionary), let loc_Arr = query_Dict["pages"]{
                            if let pages = loc_Arr as? [Any]{
                                
                                if pages.count == 0{
                                    self.reload()
                                }
                                self.pages_Arr = pages
                                print("pages : \(self.pages_Arr)")
                                
                                self.tbl_Pages.reloadData()
                            }
                        }else{
                            self.reload()
                        }
                    }
                }
                catch {
                }
            }
        }
        task.resume()
    }
}

extension ViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pages_Arr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! PagesCell
        
        let row = self.pages_Arr[indexPath.row] as? NSDictionary
        cell.lbl_ProfileName.text = "\(row?["title"] ?? "")"
        let terms =  row?["terms"] as? NSDictionary
        if let description = terms?["description"]{
            if let description = description as? NSArray{
                cell.lbl_ProfileDescrption.text = description.flatMap({($0 as? String)}).joined(separator: "\n\u{2022} ")
            }
        }
        if let thumbnail =  row?["thumbnail"] as? NSDictionary{
            let image_str = "\(thumbnail["source"] ?? "")"//thumbnail
           
            cell.img_Profile.setImageWith(URL.init(string: image_str), usingActivityIndicatorStyle: .gray)
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WikipediaVC") as! WikipediaVC
        let rowDict = self.pages_Arr[indexPath.row] as? NSDictionary
        vc.title_Str = rowDict?["title"] as? String
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}





