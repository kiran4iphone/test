//
//  WikipediaVC.swift
//  Test
//
//  Created by VMK IT Services PVT LTD on 29/09/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit
import WebKit
import ARKit
import AVFoundation
import MobileCoreServices


class WikipediaVC: UIViewController,WKUIDelegate,WKNavigationDelegate{
    //TODO:- IBOutet's
    @IBOutlet weak var viewForWebview: UIView!
    
    //TODO:- Varibales
    
    var webview: WKWebView?
    var title_Str: String?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- PopToViewController
    @IBAction func btn_PopVC(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    //MARK:- Instance Functions
    func loadWebView(){
        let queue = OperationQueue.main
        queue.addOperation {
            let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.viewForWebview.frame.size.height))
            webView.navigationDelegate = self
            self.viewForWebview.addSubview(webView)
            let nameStr =  (self.title_Str ?? "").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
            let str = "https://en.wikipedia.org/wiki/" + nameStr
            print("Url : \(str )")
            if let url = URL(string: str){
                webView.load(URLRequest(url: url))
            }
            
        }
    }
    //MARK:- Webview Delegates
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
     
        SwiftLoader.show(animated: true)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        SwiftLoader.hide()
     
    }
    
}










